package blackjackTest;



import java.util.ArrayList;
import java.util.Collections;



public class Deck1 {
	ArrayList<Card1> deck1 = new ArrayList<Card1>();

	public Deck1() {
		for (int i = 1; i < 14; i++) {
			this.deck1.add(new Card1(i, Suit1.CLUBS));
			this.deck1.add(new Card1(i, Suit1.HEARTS));
			this.deck1.add(new Card1(i, Suit1.DIAMONDS));
			this.deck1.add(new Card1(i, Suit1.SPADES));
		}
	}

	public void printDeck() {
		for (int i = 0; i < 51; i++) {
			System.out.println(this.deck1.get(i).getValue() + " " + this.deck1.get(i).getSuit());
		}
	}

	public Card1 draw(int i) {
		return this.deck1.get(i);
	}

	public void shuffle() {
		Collections.shuffle(this.deck1);
	}


}
