package blackjackTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class deckTest {
	ArrayList<Card1> deck1 = new ArrayList<Card1>();
	Deck1 deck = new Deck1();
	
	@Test
	void deckSizeTest() {
		// Deck har 52 kort
		Deck1 deck = new Deck1();
		assertEquals(52, deck.deck1.size(), "A deck should have 52 cards");
	}

//	@Test
//	void deckAfterShuffleTest() {
//		//Deck inte är 52
//		Deck1 deck = new Deck1();
//		assertNotEquals(51, deck.deck1.size(), "A deck should have 52 cards");
//		
//		//Deck size skall vara 52 efter shuffle
//		shuffle();
//		assertEquals(52, deck.deck1.size(), "A deck should have 52 cards after shuffled.");
//	}

	@Test
	//Om det finns duplicate?
	void duplicateTest() {
		Deck1 deck = new Deck1();
		for (int i = 0; i < 52; i++) {
			for (int j = i + 1; j < 52; j++) {
				if (deck.deck1.get(1).compare(deck.deck1.get(i), deck.deck1.get(j))) {
					
					fail("Fail?");
				
				}
			}
			
		}
	}
}
